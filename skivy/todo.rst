* PRIORITY

  * Autodetect presence of XTouch sliders vs. purely virutla sliders

  * Skivy works awkwardly when one has to dynamically change the size
    of data being passed to a widget. See hex's OrganismRenderer for
    an example of how it can be done, but that would not be obvious to
    someone else.

* ADD

  * Pause running when configure window comes up...and more generally
    figure out how the runnign simulation is going to be callable at
    high its/second (especially in a way that is compatible with
    something like the historyrenderer) -- maybe history renderer is
    not auto-updating, but is updated by some meta-call like
    skivy.simulation_iterate() or something like that?

    could have callbacks for model.iterate() and model.reset(), and
    these could be controlled by a GUI if desired.
  
  * Numbers indicating scale of plots.
  
  * SkivyControlCenter
    - iteration counter
    - central way to control graphic refresh rate as a function of the number of iterations ?


* FIX
  
  * ArrayRenderer
    - When the default mag_filter is nearest, it doesn't trigger the
      appropriate refresh of the drawing code. [ needs confirmation ]
    - Shape of numpy array needs to be flexible. Right now it is used to
      figure out how to draw it, but math may need it to be a certain way.
    - make array shader colours work more cleanly and for more modes
    - check that the renderers are sampling in a way that is independent of simulation
