#! /usr/bin/env python
from pylab import *
import pickle
from functools import partial
from random import choice
import string

DT = 0.01
frame_skip = 1
it = 0
VIABILITY_LIMIT = 5.0
H_LENGTH = 512


class Unit(object):
    def __init__(self, homeostat, index=0):
        self.h = homeostat
        self.index = index
        self.resetValues()
        self.it = 0

        self.bias = 0.0

    def bump(self, amnt):
        n = len(self.y)
        now = (n + self.it - 1) % n
        self.y[now] += amnt

    def resetValues(self):
        self.y = 1.0 * randn(H_LENGTH)
        self.draw_coordinates = np.zeros((H_LENGTH, 2))
        self.draw_coordinates[:, 0] = np.linspace(0, 1, H_LENGTH)
        self.draw_coordinates[:, 1] = self.y

    def iterate(self):
        n = len(self.y)
        now = (n + self.it) % n
        prev = (n + self.it - 1) % n

        accum = 0
        for from_i in xrange(len(self.h.units)):
            fn = self.h.fn_map[from_i][self.index]
            dy = fn(self.h.units[from_i].y[prev])
            accum += dy

        accum += self.bias  # (self.it*DT) ## external influence

        # hard edges
        self.y[now] = clip(self.y[prev] + DT * accum,
                           -1.1 * VIABILITY_LIMIT,
                           1.1 * VIABILITY_LIMIT)

        self.draw_coordinates[:, 1] = self.y

        self.it += 1

        # ## no edges NOT WORKING
        # self.y[now] = 1.1*cos(self.y[prev] + DT * accum)

    def isViable(self):
        n = len(self.y)
        now = (n + self.it - 1) % n

        if abs(self.y[now]) > VIABILITY_LIMIT:
            # print('Unit %d is not viable.' %(self.index))
            # print('self.y[%d] = %f' %(now,self.y[now]))
            return False

        return True


class Homeostat(object):
    def __init__(self, n_units=4):
        self.units = [Unit(self, index=index) for index in xrange(n_units)]
        self.resetFnMap()

    def resetFnMap(self):
        #print('RESET FUNCTION MAP. iteration=%d'%(self.it))
        # allowed functions and number of randomized parameters to be taken
        fns = [  # (sin,0),
            #(cos,0),
            #(exp,0),
            #(lambda p1,p2,x: cos(p1+x*p2) ,2),  ## linear scale
            (lambda p, x: ((10.0 * p) - 5.0) * x, 1),  # linear scale
            #(lambda p,x: pow(x,abs(p)),1), ## power scale
        ]

        def getRandomFn():
            fn = choice(fns)
            if fn[1] == 1:
                p1 = rand()
                fn = partial(fn[0], p1)
            elif fn[1] == 2:
                p1 = rand()
                p2 = rand()
                fn = partial(fn[0], p1, p2)
            else:
                fn = fn[0]
            return fn

        # from, to
        self.fn_map = [[getRandomFn() for f in self.units] for t in self.units]

    def iterate(self):
        # call iterate on all units
        map(lambda u: u.iterate(), self.units)

        if not all([u.isViable() for u in self.units]):
            self.resetFnMap()

    def reset(self):
        self.resetFnMap()
        map(lambda u: u.resetValues(), self.units)

# h = Homeostat()

# def iterate() :
#     self.it += 1
#     for _ in xrange(g.frame_skip) :
#         h.iterate()

# def reset() :
#     self.it = 0
#     h.reset()
#     pass

# def setExternalSinusoidalAmplitude(amplitude) :
#     h.units[0].bias = lambda t : amplitude#*sin(0.5*t)

# def removeInfluencesFrom(unit_i) :
#     h.units[0].bias = lambda t : 0.0

# def setExternalConstant(k) :
#     h.units[0].bias = lambda t : k

# graphics.run({'iterate':iterate,
#               'reset':reset,
#               'setExternalConstant': setExternalConstant,
#               'setExternalSinusoidalAmplitude': setExternalSinusoidalAmplitude,
#               'removeInfluencesFrom':removeInfluencesFrom})
