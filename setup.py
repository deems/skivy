from setuptools import setup

setup(name='skivy',
      version='0.1',
      description='Realtime interaction and visualization toolkit for scientists',
      url='http://matthewegbert.com',
      author='Matthew Egbert',
      author_email='mde@matthewegbert.com',
      license='MIT',
      packages=['skivy'],
      zip_safe=False)
