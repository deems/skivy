import re
import os
from jinja2 import Environment, PackageLoader, FileSystemLoader
#env = Environment(loader=PackageLoader('skivy', 'shaders'))
module_path = os.path.dirname(os.path.realpath(__file__))
env = Environment(loader=FileSystemLoader([module_path + '/shaders', '.']))


def loadShaders(fn, substitutions):
    # for x in xrange(20) :
    #     print(fn)
    #     print(env.loader)
    template = env.get_template(fn)
    shader_text = template.render(substitutions)
    ss = shader_text.split('---VERTEX SHADER---')
    ss = ss[1].split('---FRAGMENT SHADER---')
    vertex_shader = str(ss[0])
    frag_shader = str(ss[1])
    return {'vs': vertex_shader,
            'fs': frag_shader}
