from kivy import platform
from kivy.config import Config

# TODO: Make graphs move rather than being overwritten (like earthquakeometers)
# TODO: Add buttons to allow toggleing between different input modes.

if platform == 'linux':
    ratio = 2.0
    # w = 1920/2
    w = 1400
    Config.set('graphics', 'width', str(int(w)))
    Config.set('graphics', 'height', str(int(w / 2)))
    # Config.set('graphics', 'fullscreen', 'auto')

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.lang import Builder

import numpy as np
import skivy

DT = 0.02


class CTRNNNode(object):
    def __init__(self):
        self.HISTORY_LENGTH = 100

        self.iteration = 0
        self.bias = 0.0
        self.tau = 1.0
        self.y = 1.0

        self.dydt = 0.0
        self.input = 0.0
        self.presynaptic_nodes = []
        self.synaptic_weights = []

    @staticmethod
    def sigmoid(x):
        return -0.5 + 1.0 / (1.0 + np.exp(-x))

    def get_output(self):
        return self.y - self.bias

    def prepare_to_update(self):
        sigmoid_term = sum([weight * self.sigmoid(inp.get_output())
                            for inp, weight in zip(self.presynaptic_nodes, self.synaptic_weights)])

        self.dydt = (-self.y + sigmoid_term + self.input) / self.tau

    def update(self):
        self.y += self.dydt * DT


class Model(Widget):
    def __init__(self, *args, **kwargs):
        self.input_form = 'sin'
        self.it = 0

        self.M_bias = 0.0
        self.M_tau = 1.0
        self.M_weight = 1.0
        self.M_weight2 = 1.0

        self.sin_drive_frequency = 1.0
        self.input = 0.0
        self.input_weight = 1.0
        self.node_1 = CTRNNNode()
        self.node_2 = CTRNNNode()
        self.node_3 = CTRNNNode()

        self.node_2.presynaptic_nodes.append(self.node_1)
        self.node_2.synaptic_weights = [self.M_weight]

        self.node_3.presynaptic_nodes.extend([self.node_1, self.node_2])
        self.node_3.synaptic_weights = [self.M_weight, self.M_weight2]

        # "manual nodes" are nodes for which the sliders influence the parameters
        self.manual_nodes = [self.node_1]
        self.all_nodes = [self.node_1, self.node_2, self.node_3]

        super(Model, self).__init__(**kwargs)

        def iterate(arg):
            self.iterate()

        Clock.schedule_interval(iterate, 1.0 / 500.0)

    def iterate(self):
        if self.input_form == 'sin':
            self.input = np.sin(self.it * DT * 2.0 * np.pi / self.sin_drive_frequency)
        elif self.input_form == 'pulse':
            self.input = np.sin(self.it * DT * 2.0 * np.pi / self.sin_drive_frequency) > 0.8
        self.node_1.input = self.input_weight * self.input
        self.node_2.synaptic_weights = [self.M_weight]
        self.node_3.synaptic_weights = [self.M_weight, self.M_weight2]

        for n in self.manual_nodes:
            n.bias = self.M_bias
            n.tau = self.M_tau

        for n in self.all_nodes:
            n.prepare_to_update()
        for n in self.all_nodes:
            n.update()

        self.it += 1

    def set_input_form(self, input_form):
        self.input_form = input_form


class ModelApp(App):
    def build(self):
        return Builder.load_file('sk.kv')

    def on_stop(self):
        skivy.disactivate()


if __name__ == '__main__':
    skivy.activate()
    ModelApp().run()
