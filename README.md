## Package Installation

To install this package in development mode (where changes to skivy
apply immediately to your skivy-using programs, run the following
command from the /skivy/ root folder.

-`pip install -e .`

Note that currently skivy is not compatible with Python3.

## Running kuramoto example
After installing the package, run:

-`pip install cython`
-`pip install kivy`

I found that on Debian distros, the last command fails, and the below should be used instead running it:

-`apt-get install python-kivy python-kivy-examples debhelper python python-all-dev cython libgl1-mesa-dev libgles2-mesa-dev`

And then proceed instally Pydot:

-`pip install pydot`

Again, Debian distros require special treatment:

-`apt install python-pydot-ng graphviz`

And manually install pydot from here: 

-`https://pypi.python.org/pypi/pydot`
