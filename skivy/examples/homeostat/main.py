from kivy import platform
from kivy.config import Config
from homeostat import *

if platform == 'linux':
    ratio = 2.0
    # w = 1920/2
    w = 1400
    Config.set('graphics', 'width', str(int(w)))
    Config.set('graphics', 'height', str(int(w / 2)))
    Config.set('graphics', 'fullscreen', 'auto')

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.lang import Builder

import numpy as np
import skivy


class Model(Widget):
    def __init__(self, *args, **kwargs):
        self.h = Homeostat()
        self.it = 0
        self.its_per_frame = 1

        super(Model, self).__init__(**kwargs)

        def iterate(arg):
            self.iterate()

        Clock.schedule_interval(iterate, 1.0 / 500.0)

    def setItsPerFrame(self, ipf):
        self.its_per_frame = ipf

    def iterate(self):
        for _ in xrange(self.its_per_frame):
            self.h.iterate()
        self.it += 1


class ModelApp(App):
    def build(self):
        return Builder.load_file('sk.kv')

    def on_stop(self):
        skivy.disactivate()


if __name__ == '__main__':
    skivy.activate()
    ModelApp().run()
